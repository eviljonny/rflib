from distutils.core import setup, Extension

extension_mod = Extension(
    "rflib",
    libraries=['wiringPi'],
    sources=['rflib.cpp', 'RCSwitch.cpp'],
)

setup(name="rflib", ext_modules=[extension_mod])

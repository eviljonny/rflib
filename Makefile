all: rflib

rflib:
	python2.7 setup.py build
	
clean:
	$(RM) *.o
	$(RM) -rf build/

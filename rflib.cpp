extern "C" {
    #include "RCSwitch.h"
    #include "Python.h"
}

extern "C" {
    static PyObject *
    rflib_raiseError(PyObject *self, PyObject *args)
    {
        PyErr_SetString(PyExc_RuntimeError, "wiringPi setup failed");
        return NULL;
    }
}

extern "C" {
    static PyObject *
    rflib_sendcode(PyObject *self, PyObject *args)
    {
        long long code;
        int pin;

        if (!PyArg_ParseTuple(args, "iL", &pin, &code))
            return NULL;

        if (wiringPiSetup() == -1) {
            PyErr_SetString(PyExc_RuntimeError, "wiringPi setup failed");
            return NULL;
        }

        RCSwitch sender = RCSwitch();
        sender.enableTransmit(pin);

        sender.send(code, 24);

        return Py_BuildValue("i", 1);
    }
}

static PyMethodDef rflibMethods[] = {
    {"sendcode", rflib_sendcode, METH_VARARGS,
     "Send an RF code"},
    {"raiseError", rflib_raiseError, METH_VARARGS,
     "Raise a runtime exception"},
    {NULL, NULL, 0, NULL} /* Sentinel */
};

extern "C" {
    PyMODINIT_FUNC
    initrflib(void)
    {
        (void) Py_InitModule("rflib", rflibMethods);
    }

}

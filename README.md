RFLib is a Python C++ extension which allows you to use the RCSwitch 

Building
========

    make clean
    make

Dependencies
============

* wiringPi (http://wiringpi.com/)
* python-dev

Copyright
=========

This project is released under GNU LGPL v2.1 (Included in the project in the
LICENSE file)

RCSwitch - Released under GPL v2.1 (a copy of which is included in the LICENSE
file in this project), taken from the NinjaBlocks 433Utils package
(https://github.com/ninjablocks/433Utils/blob/master/RPi\_utils/RCSwitch.cpp).
Originally from the rc-switch project (http://code.google.com/p/rc-switch/).
